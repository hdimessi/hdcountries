//
//  MenuVC.m
//  HDCountries
//
//  Created by Hussein Dimessi on 05/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import "MenuVC.h"

@interface MenuVC ()

@end

@implementation MenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)openCountries:(UIButton *)sender {
    [APP_DELEGATE.drawer openNavigationBranch:@"CountriesListVC"];
}

- (IBAction)openAnimations:(UIButton *)sender {
    [APP_DELEGATE.drawer openNavigationBranch:@"AnimationsVC"];
}


@end
