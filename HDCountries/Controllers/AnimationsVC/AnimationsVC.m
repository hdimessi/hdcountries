//
//  AnimationsVC.m
//  HDCountries
//
//  Created by Hussein Dimessi on 12/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import "AnimationsVC.h"
#import "HGCFlagholderIndicator.h"
#import "BookmarkAnimationView.h"

@interface AnimationsVC ()

@end

@implementation AnimationsVC {
    HGCFlagholderIndicator *loader;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.backgroundColor = [UIColor orangeColor];
    self.navigationController.navigationBar.tintColor = [UIColor orangeColor];
    
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-open-icon" ] style:UIBarButtonItemStylePlain target:self action:@selector(openMenu)];
    self.navigationItem.leftBarButtonItem = menuButton;
    
    loader = [[HGCFlagholderIndicator alloc]initWithFrame:CGRectMake(0, 80, SCREEN_SIZE.width, 60)];
    
    [self.view addSubview:loader];
    [loader startLoading];
}

-(void)viewWillAppear:(BOOL)animated {
    [HDCActivityIndicator.shared stop];
    [HDCActivityIndicator.shared startLoadingInView:self.view];
    
    [self.navigationController.navigationBar setHidden:false];
}

-(void)viewWillDisappear:(BOOL)animated {
    [self.navigationController.navigationBar setHidden:true];
}

- (void)openMenu {
    [APP_DELEGATE.drawer openMenu];
    
}

- (IBAction)bookmark:(UIButton *)sender {
    BookmarkAnimationView *v = [[BookmarkAnimationView alloc]init];
    [v bookmarkIt];
}

@end
