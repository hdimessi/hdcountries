//
//  CountriesListVC.m
//  HDCountries
//
//  Created by Hussein Dimessi on 05/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import "CountriesListVC.h"
#import "CountryCell.h"
#import "HDRequest.h"
#import "Country.h"
#import "DetailsVC.h"

@interface CountriesListVC ()

@end

@implementation CountriesListVC {
    NSArray *fullList;
}

@synthesize countries;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    UIImageView *blackLoop = [[UIImageView alloc]initWithFrame:CGRectMake(4, 0, 18, 20)];
    blackLoop.image = [UIImage imageNamed:@"black-loop"];
    blackLoop.contentMode = UIViewContentModeScaleAspectFit;
    blackLoop.alpha = 0.8;
    [leftView addSubview:blackLoop];
    _searchTextField.leftView = leftView;
    _searchTextField.leftViewMode = UITextFieldViewModeAlways;
    
    [_tableView registerNib:[UINib nibWithNibName:@"CountryCell" bundle:nil] forCellReuseIdentifier:@"CountryCell"];
    [self setupKeyboardManagement];
    
    
    
    
    HDRequest *request = [[HDRequest alloc]init];
    __weak CountriesListVC *weakSelf = self;
    NSURL *url = [NSURL URLWithString:@"https://restcountries.eu/rest/v2/all"];
    [HDCActivityIndicator.shared startLoadingInView:self.view];
    [request getJSONWithURL:url parameters:nil completion:^(NSArray *json, NSError *error) {
        [HDCActivityIndicator.shared stop];
        if (json) {
            fullList = [Country parseCountries:json];
            weakSelf.countries = fullList;
            [weakSelf.tableView reloadData];
        }
    }];
    
    
}

- (IBAction)openMenu:(UIButton *)sender {
    [APP_DELEGATE.drawer openMenu];
    
}

- (IBAction)search:(UIButton *)sender {
    [_searchTextField becomeFirstResponder];
    
}



#pragma mark - TextField delegate

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    countries = fullList;
    [_tableView reloadData];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [_searchTextField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *fullString;
    
    if (range.length == 0) {
        fullString = [NSString stringWithFormat:@"%@%@",textField.text, string];
    } else if((range.length == 1) && textField.text.length > 1) {
        fullString = [textField.text substringToIndex:textField.text.length - 1];
    }
    
    countries = [self filterCountriesArray:fullList ByName:fullString];
    [_tableView reloadData];
    
    
    return YES;
}

- (IBAction)hideKeyboard:(UITapGestureRecognizer *)sender {
    [self.view endEditing:true];
}

#pragma mark - keyboard/search texfield management

-(void)setupKeyboardManagement {
    [self.view addConstraint:_hideSearchField];
    [_hideKeyboardTap setEnabled: false];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide) name:UIKeyboardWillHideNotification object:nil];
}

-(void)keyboardWillShow {
    [_hideKeyboardTap setEnabled: true];
    [UIView animateWithDuration:0.5 animations:^{
        [self.view removeConstraint:_hideSearchField];
        [self.view layoutIfNeeded];
    }];
}

-(void)keyboardWillHide {
    [_hideKeyboardTap setEnabled: false];
    if (![self.view.constraints containsObject:_hideSearchField]) {
        [UIView animateWithDuration:0.5 animations:^{
            [self.view addConstraint:_hideSearchField];
            [self.view layoutIfNeeded];
        }];
    }
}

#pragma mark - tableview datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return countries.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CountryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CountryCell" forIndexPath:indexPath];
    Country *country = countries[indexPath.row];
    [cell setupCellWithCountry:country];
    
    return cell;
}

#pragma mark - tableview delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 180;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DetailsVC *detailsVC = [[DetailsVC alloc]initWithCountry:countries[indexPath.row]];
    [self presentViewController:detailsVC animated:true completion:nil];
}

#pragma mark - filter list

-(NSArray*)filterCountriesArray:(NSArray*)array ByName:(NSString*)name {
    if (!name) {
        return fullList;
    }
    NSMutableArray *resultArray = [[NSMutableArray alloc]init];
    for (Country *country in array) {
        if ([[country.name lowercaseString] containsString:[name lowercaseString]]) {
            [resultArray addObject:country];
        }
    }
    
    return resultArray;
}

@end



























