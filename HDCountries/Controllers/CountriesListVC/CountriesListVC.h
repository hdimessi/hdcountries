//
//  CountriesListVC.h
//  HDCountries
//
//  Created by Hussein Dimessi on 05/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountriesListVC : UIViewController <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *hideSearchField;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *hideKeyboardTap;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) NSArray *countries;


@end
