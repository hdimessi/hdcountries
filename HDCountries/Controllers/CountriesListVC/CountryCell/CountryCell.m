//
//  CountryCell.m
//  HDCountries
//
//  Created by Hussein Dimessi on 6/5/17.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import "CountryCell.h"
#import "HGCFlagholderIndicator.h"

@implementation CountryCell {
    HDRequest *imageDownloader;
    HGCFlagholderIndicator *loader;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    imageDownloader = [[HDRequest alloc]init];
    loader = [[HGCFlagholderIndicator alloc]initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 155)];
    [self.contentView addSubview:loader];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    [_unavailable setHidden:true];
}

-(void)prepareForReuse {
    [_unavailable setHidden:true];
    [imageDownloader cancelRequest];
    _flagImage.image = nil;
    [loader startLoading];
}

-(void)setupCellWithCountry:(Country*)country {
    _countryName.text = country.name;
    NSURL *imgUrl = [NSURL URLWithString:country.flagLink];
    
    if (imgUrl) {
        __weak CountryCell *weakSelf = self;
        [imageDownloader downloadImageWithURL:imgUrl completion:^(UIImage *image, NSError *error) {
            [loader stop];
            if (image) {
                weakSelf.flagImage.image = image;
                [_unavailable setHidden:true];
            }else{
                [_unavailable setHidden:false];
            }
        }];
    }
}

@end
