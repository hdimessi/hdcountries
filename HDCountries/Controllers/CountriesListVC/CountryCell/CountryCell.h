//
//  CountryCell.h
//  HDCountries
//
//  Created by Hussein Dimessi on 6/5/17.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Country.h"

@interface CountryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *flagImage;
@property (weak, nonatomic) IBOutlet UILabel *countryName;
@property (weak, nonatomic) IBOutlet UILabel *unavailable;

-(void)setupCellWithCountry:(Country*)country;

@end
