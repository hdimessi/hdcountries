//
//  DetailsVC.h
//  HDCountries
//
//  Created by Hussein Dimessi on 07/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Country.h"

@interface DetailsVC : UIViewController

- (instancetype)initWithCountry:(Country*)country;

@property (weak, nonatomic) IBOutlet UILabel *region;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *capital;
@property (weak, nonatomic) IBOutlet UILabel *population;
@property (weak, nonatomic) IBOutlet UIImageView *flag;
@property (weak, nonatomic) IBOutlet MKMapView *map;

@property Country *country;

@end
