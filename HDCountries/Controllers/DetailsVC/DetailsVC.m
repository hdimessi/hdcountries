//
//  DetailsVC.m
//  HDCountries
//
//  Created by Hussein Dimessi on 07/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import "DetailsVC.h"
#import "HGCFlagholderIndicator.h"
#import "BookmarkAnimationView.h"

@interface DetailsVC ()

@end

@implementation DetailsVC


- (instancetype)initWithCountry:(Country*)country {
    self = [self initWithNibName:@"DetailsVC" bundle:nil];
    self.country = country;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _name.text = _country.name;
    _region.text = _country.region;
    _capital.text = _country.capital;
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
    [formatter setGroupingSeparator:groupingSeparator];
    [formatter setGroupingSize:3];
    [formatter setAlwaysShowsDecimalSeparator:NO];
    [formatter setUsesGroupingSeparator:YES];
    NSString *formattedPopulation = [formatter stringFromNumber:[NSNumber numberWithFloat:_country.population]];
    _population.text = [NSString stringWithFormat:@"%@", formattedPopulation];
    
    double span = sqrt(_country.area) / 111;
    [_map setRegion:MKCoordinateRegionMake(CLLocationCoordinate2DMake(_country.latitude, _country.longitude), MKCoordinateSpanMake(span, span))];
    
    NSURL *imgUrl = [NSURL URLWithString:_country.flagLink];
    
    if (imgUrl) {
        HGCFlagholderIndicator *loader = [[HGCFlagholderIndicator alloc]initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 155)];
        loader.center = _flag.center;
        [self.view addSubview:loader];
        __weak DetailsVC *weakSelf = self;
        HDRequest *imageDownloader = [[HDRequest alloc]init];
        [loader startLoading];
        [imageDownloader downloadImageWithURL:imgUrl completion:^(UIImage *image, NSError *error) {
            [loader stop];
            if (image) {
                weakSelf.flag.image = image;
            }
        }];
    }
}

- (IBAction)close:(UIButton *)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}


- (IBAction)bookmark:(UIButton *)sender {
    BookmarkAnimationView *v = [[BookmarkAnimationView alloc]init];
    [v bookmarkIt];
}


@end
