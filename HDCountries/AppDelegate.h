//
//  AppDelegate.h
//  HDCountries
//
//  Created by Hussein Dimessi on 05/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "HDDrawer.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property HDDrawer *drawer;
@property UINavigationController *rootNavigationController;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

