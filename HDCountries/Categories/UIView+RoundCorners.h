//
//  UIView+RoundCorners.h
//  HDCountries
//
//  Created by Hussein Dimessi on 12/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RoundCorners)

@property(nonatomic, assign) BOOL roundCorners;

@end
