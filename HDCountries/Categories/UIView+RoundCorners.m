//
//  UIView+RoundCorners.m
//  HDCountries
//
//  Created by Hussein Dimessi on 12/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import "UIView+RoundCorners.h"

@implementation UIView (RoundCorners)

- (void) setRoundCorners:(BOOL) roundCorners {
    if (roundCorners) {
        self.layer.cornerRadius = self.frame.size.height / 2;
    }
}

@end
