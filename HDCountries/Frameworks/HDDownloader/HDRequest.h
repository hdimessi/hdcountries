//
//  HDRequest.h
//  HDDownloader
//
//  Created by Hussein Dimessi on 4/14/17.
//  Copyright © 2017 Hussein Dimessi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Protocols.h"

/**
 Enumeration that define the type of error incase a request fails
 */
typedef NS_ENUM(NSInteger, HDRequestError) {
    HDRequestErrorUnexepectedContent,
    HDRequestErrorNoInternet,
    HDRequestErrorTimedOut,
    HDRequestErrorAlreadyDownloading,
    HDRequestErrorUnsupportedURL,
    HDRequestErrorCancelled
};

/**
 Enumeration that define the status of a request
 */
typedef NS_ENUM(NSInteger, HDRequestStatus) {
    HDRequestStatusIdle,
    HDRequestStatusPending,
    HDRequestStatusDownloading,
    HDRequestStatusFinished,
    HDRequestStatusCanceled
};

typedef NS_ENUM(NSInteger, HttpMethod) {
    GET,
    POST,
    PUT,
    DELETE
};

typedef void (^ImageCompletionBlock)(UIImage *image, NSError *error);
typedef void (^JSONCompletionBlock)(NSArray *json, NSError *error);
typedef void (^DataCompletionBlock)(NSData *data, NSError *error);
typedef void (^CompletionBlock)(id any, NSError *error);


@interface HDRequest : NSObject

@property (readonly) HDRequestStatus status;

@property (nonatomic) NSDictionary *headerData;

@property (readonly) NSDictionary *parameters;

@property (readonly) HttpMethod method;

@property (readonly) NSURL *requestUrl;

@property (readonly) NSInteger priority;

-(void)downloadImageWithURL:(NSURL *)url WithPriority:(NSNumber *)priority completion:(ImageCompletionBlock) completion;
-(void)downloadImageWithURL:(NSURL *)url completion:(ImageCompletionBlock) completion;

-(void)getJSONWithURL:(NSURL *)url parameters:(NSDictionary*)parameters
         WithPriority:(NSNumber *)priority completion:(JSONCompletionBlock) completion;
-(void)getJSONWithURL:(NSURL *)url parameters:(NSDictionary*)parameters
           completion:(JSONCompletionBlock) completion;

-(void)postJSONWithURL:(NSURL *)url parameters:(NSDictionary*)parameters
         WithPriority:(NSNumber *)priority completion:(JSONCompletionBlock) completion;
-(void)postJSONWithURL:(NSURL *)url parameters:(NSDictionary*)parameters
            completion:(JSONCompletionBlock) completion;

-(void)deleteJSONWithURL:(NSURL *)url parameters:(NSDictionary*)parameters
           completion:(JSONCompletionBlock) completion;

//this last one can obviously be used for anything (xml, pdf ...)
-(void)downloadDataWithURL:(NSURL *)url parameters:(NSDictionary*)parameters
          WithPriority:(NSNumber *)priority completion:(DataCompletionBlock) completion;
-(void)downloadDataWithURL:(NSURL *)url parameters:(NSDictionary*)parameters
               completion:(DataCompletionBlock) completion;
-(void)downloadDataWithURL:(NSURL *)url WithPriority:(NSNumber *)priority
                completion:(DataCompletionBlock) completion;
-(void)downloadDataWithURL:(NSURL *)url completion:(DataCompletionBlock) completion;



-(void)cancelRequest;

+(void)setMaxSimultaneousRequest:(NSInteger)max; //this would be effected by the request's priority for example if you set it to 10 you can only make 5 simultaneous requests with priority of 2 or 10 with standard priority of 1 , or only one request with priority of 10

+(void)cancelAllRequests; //cancel all networking requests (ongoing/pending)

+(void)setTimeOut:(NSInteger)timeout;

+(void)setMaxCacheSizeInBytes:(NSInteger)size; //Default is at 100MB

@property (weak, readonly) id<HDRequestDelegate> delegate;

@end
