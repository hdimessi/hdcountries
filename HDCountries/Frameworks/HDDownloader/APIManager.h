//
//  APIManager.h
//  HDDownloader
//
//  Created by Hussein Dimessi on 4/14/17.
//  Copyright © 2017 Hussein Dimessi. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HDRequest;

typedef void (^APICompletionBlock)(NSData *data, NSURLResponse *response, NSError *error);

@interface APIManager : NSObject

+(APIManager*)sharedManager;

@property NSInteger timeout;

@property NSDictionary *headerData;

@property (nonatomic) NSInteger maxTotalPriorities;

@property (nonatomic) NSCache *cache;

-(void)appendNewRequest:(HDRequest *)request completion:(APICompletionBlock) completion;

-(void)requestApiWithHttpMethod:(NSInteger)method url:(NSURL *)url
                     parameters:(NSDictionary *)params customHeaders:(NSDictionary*)headers
                     completion:(APICompletionBlock)completion;

-(void)cancelAllRequests;

@end
