//
//  NSString+Encoding.m
//  HDDownloader
//
//  Created by Hussein Dimessi on 4/14/17.
//  Copyright © 2017 Hussein Dimessi. All rights reserved.
//

#import "NSString+Encoding.h"

@implementation NSString (Encoding)

-(NSString *)addingPercentEncodingForURLQueryValue {
    NSString *allowedChars = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~";
    NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:allowedChars];
    
    return [self stringByAddingPercentEncodingWithAllowedCharacters:charSet];
}

@end
