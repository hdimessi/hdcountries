//
//  NSDictionary+HTTP.m
//  HDDownloader
//
//  Created by Hussein Dimessi on 4/14/17.
//  Copyright © 2017 Hussein Dimessi. All rights reserved.
//

#import "NSDictionary+HTTP.h"
#import "NSString+Encoding.h"

@implementation NSDictionary (HTTP)

-(NSString *)stringFromHttpParameters {
    NSString *result =@"";
    for (NSString *key in self.allKeys) {
        if (![key isKindOfClass:NSString.class]) {
//            NSLog(@"key not a string");
            continue;
        }
        NSString *percentEscapedKey = [key addingPercentEncodingForURLQueryValue];
        if (!percentEscapedKey) {
            continue;
        }
        id value = [self objectForKey:key];
        NSString *stringValue;
        if ([value isKindOfClass:NSString.class] || [value isKindOfClass:NSNumber.class]) {
            stringValue = [[value description] addingPercentEncodingForURLQueryValue] ;
        }else if ([NSJSONSerialization isValidJSONObject:value]){
            NSError *error;
            NSData *units = [NSJSONSerialization dataWithJSONObject:value options:0 error:&error];
            if (error) {
                continue;
            }
            stringValue = [[NSString alloc] initWithData:units encoding:NSUTF8StringEncoding];
            if (!stringValue) {
                continue;
            }
        }else{
            continue;
        }
        if (result.length > 0) {
            result = [NSString stringWithFormat:@"%@&" ,result];
        }
        result = [NSString stringWithFormat:@"%@%@=%@" ,result ,key ,stringValue];
        
    }
    return result;
}

@end
