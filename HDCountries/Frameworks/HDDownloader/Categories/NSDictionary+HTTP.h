//
//  NSDictionary+HTTP.h
//  HDDownloader
//
//  Created by Hussein Dimessi on 4/14/17.
//  Copyright © 2017 Hussein Dimessi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (HTTP)

-(NSString *)stringFromHttpParameters ;

@end
