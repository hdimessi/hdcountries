//
//  HDDownloader.h
//  HDDownloader
//
//  Created by Hussein Dimessi on 4/14/17.
//  Copyright © 2017 Hussein Dimessi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HDDownloader/HDRequest.h>

//! Project version number for HDDownloader.
FOUNDATION_EXPORT double HDDownloaderVersionNumber;

//! Project version string for HDDownloader.
FOUNDATION_EXPORT const unsigned char HDDownloaderVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HDDownloader/PublicHeader.h>


