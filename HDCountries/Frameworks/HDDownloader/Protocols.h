//
//  Protocols.h
//  HDDownloader
//
//  Created by Hussein Dimessi on 4/16/17.
//  Copyright © 2017 Hussein Dimessi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HDRequest;

@protocol HDRequestDelegate <NSObject>

@optional

/** Notifies the delegate that the request began downloading.
 */

- (void)requestBeganDownloading ;

@end

