//
//  APIManager.m
//  HDDownloader
//
//  Created by Hussein Dimessi on 4/14/17.
//  Copyright © 2017 Hussein Dimessi. All rights reserved.
//

#import "APIManager.h"
#import "HDRequest.h"
#import "NSDictionary+HTTP.h"

@interface APIManager ()

@property (nonatomic, setter=setHighestPriority:) NSInteger highestPriority;
@property (nonatomic) NSInteger currentTotalPriorities;
@property (nonatomic, setter=setIsBusy:) BOOL isBusy;

@property (nonatomic) NSMutableDictionary *requests;
@property (nonatomic) NSMutableDictionary *sortedRequests;

@end

@implementation APIManager

@synthesize headerData, timeout, requests, sortedRequests;

+ (APIManager *)sharedManager {
    static APIManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

-(instancetype)init {
    self = [super init];
    [self setTimeout:5];
    requests = [[NSMutableDictionary alloc]init];
    sortedRequests = [[NSMutableDictionary alloc]init];
    [self setHighestPriority:0];
    _maxTotalPriorities = 10;
    _currentTotalPriorities = 0;
    NSLog(@"%ld", (long)_highestPriority);
    _isBusy = false;
    self.cache = [[NSCache alloc]init];
    [self.cache setTotalCostLimit:100000000];
    return self;
}

-(void)setHighestPriority:(NSInteger)highestPriority{
    _highestPriority = highestPriority;
    if (!_isBusy && _highestPriority > 0) {
        [self startHustling];
    }
}

-(void)setIsBusy:(BOOL)isBusy {
    _isBusy = isBusy;
}

-(void)appendNewRequest:(HDRequest *)request completion:(APICompletionBlock) completion {
    NSMutableDictionary *ongoingOrPendingRequest = [requests objectForKey:request.requestUrl.absoluteString];
    HDRequest *similarRequest;
    if (ongoingOrPendingRequest) {
        NSArray *similarRequests = ongoingOrPendingRequest[@"requests"];
        similarRequest = similarRequests[0];
    }
    if (ongoingOrPendingRequest
        && [request.parameters isEqualToDictionary:similarRequest.parameters]
        && request.method == similarRequest.method) {
        // if there s a pending/ongoing request with the same url just add this new completion block to be called when the other one finishes
        
        NSMutableArray *completionBlocks = [ongoingOrPendingRequest objectForKey:@"completionBlocks"];
        [completionBlocks addObject:completion];
        NSMutableArray *formerRequests = [ongoingOrPendingRequest objectForKey:@"requests"];
        [formerRequests addObject:request];
        
        NSNumber *status = [ongoingOrPendingRequest objectForKey:@"status"];
        NSNumber *oldPriority = [ongoingOrPendingRequest objectForKey:@"priority"];
        if (status.integerValue == 0 && request.priority > oldPriority.integerValue) {
            //pending request needs to update the priority
            [ongoingOrPendingRequest setObject:@(request.priority) forKey:@"priority"];
            
            //get the array of the requests with the same priority
            NSMutableArray *prioritisedRequests = [sortedRequests objectForKey:@(request.priority)];
            if (!prioritisedRequests) {
                prioritisedRequests = [[NSMutableArray alloc]init];
            }
            [prioritisedRequests addObject:ongoingOrPendingRequest];
            [sortedRequests setObject:prioritisedRequests forKey:@(request.priority)];
            
            //remove the request from the array with the old priority
            NSMutableArray *oldPrioritisedRequests = [sortedRequests objectForKey:oldPriority];
            [oldPrioritisedRequests removeObject:ongoingOrPendingRequest];
            if (oldPrioritisedRequests.count == 0) {
                [sortedRequests removeObjectForKey:oldPriority];
            }
            
            //finally update the highestPriority if need be
            if (request.priority > _highestPriority) {
                _highestPriority = request.priority;
            }
        }
        
        if (status.integerValue == 1) {
            //ongoing request need to update the newRequest status
            if([request.delegate respondsToSelector:@selector(requestBeganDownloading)]) {
                [request.delegate requestBeganDownloading];
            }
        }
    }else{
        // new request
        NSMutableDictionary *newRequest = [[NSMutableDictionary alloc]init];
        NSNumber *priority = @(request.priority);
        newRequest[@"priority"] = priority;
        newRequest[@"completionBlocks"] = [[NSMutableArray alloc]initWithObjects:completion, nil];
        newRequest[@"status"] = @(0);
        newRequest[@"requests"] = [[NSMutableArray alloc]initWithObjects:request, nil];
        
        //get the array of the requests with the same priority
        NSMutableArray *prioritisedRequests = [sortedRequests objectForKey:priority];
        if (!prioritisedRequests) {
            prioritisedRequests = [[NSMutableArray alloc]init];
        }
        
        [prioritisedRequests addObject:newRequest];
        [sortedRequests setObject:prioritisedRequests forKey:@(request.priority)];
        [requests setObject:newRequest forKey:request.requestUrl.absoluteString];
        
        //finally update the highestPriority if need be
        if (request.priority > _highestPriority) {
            [self setHighestPriority:request.priority];
        }
    }
}

-(void)requestApiWithHttpMethod:(NSInteger)method url:(NSURL *)url
                     parameters:(NSDictionary *)params customHeaders:(NSDictionary*)headers
                     completion:(APICompletionBlock)completion
{
    NSURLSession *sharedSession = [NSURLSession sharedSession];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSString *stringParameters = [params stringFromHttpParameters];
    switch (method) {
        case 0: //GET
        {
            if (stringParameters) {
                url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@?%@", url.absoluteString, stringParameters] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
                request.URL = url;
            }
            [request setHTTPMethod:@"GET"];
            break;
        }
        case 1: //POST
        {
            [request setHTTPMethod:@"POST"];
            if (stringParameters) {
                [request setHTTPBody:[stringParameters dataUsingEncoding:NSUTF8StringEncoding]];
            }
            break;
        }
        case 3: //POST
        {
            [request setHTTPMethod:@"DELETE"];
            break;
        }
        default:
            break;
    }
    
    [request setTimeoutInterval:timeout];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    if (headers) {
        for (NSString *key in headers.allKeys) {
            [request addValue:[headers objectForKey:key] forHTTPHeaderField:key];
        }
    }else{
        for (NSString *key in headerData.allKeys) {
            [request addValue:[headerData objectForKey:key] forHTTPHeaderField:key];
        }
    }
    
    NSURLSessionDataTask *dataTask = [sharedSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        completion(data, response, error);
    }];
    [dataTask resume];
}

//sorry for the method name was gonna change it but it just felt right XD
-(void)startHustling {
    if (_currentTotalPriorities >= _maxTotalPriorities || _highestPriority == 0) {
        return;
    }
    //get the request with the highest priority
    __block NSMutableArray *highestPriorityRequestArray = [sortedRequests objectForKey:@(_highestPriority)];
    __block NSMutableDictionary *request = highestPriorityRequestArray[0]; //oldest request
    NSMutableArray *hdRequests = request[@"requests"];
    
    BOOL procced = NO;
    for (HDRequest *hdRequest in hdRequests) {
        if (hdRequest.status == HDRequestStatusPending) {
            procced = YES;
            if([hdRequest.delegate respondsToSelector:@selector(requestBeganDownloading)]) {
                [hdRequest.delegate requestBeganDownloading];
            }
        }
    }
    HDRequest *firstRequest = hdRequests[0];
    if (!procced) {
        [self.requests removeObjectForKey:firstRequest.requestUrl.absoluteString];
        [highestPriorityRequestArray removeObjectAtIndex:0];
        
        if (highestPriorityRequestArray.count == 0) {
            [self.sortedRequests removeObjectForKey:@(self.highestPriority)];
            if (self.sortedRequests.count > 0) {
                NSInteger tempHighestPrio = 0;
                for (NSNumber *prio in self.sortedRequests.allKeys) {
                    if (prio.integerValue > tempHighestPrio) {
                        tempHighestPrio = prio.integerValue;
                    }
                }
                [self setHighestPriority:tempHighestPrio];
            }
        }else{
            [self.sortedRequests setObject:highestPriorityRequestArray forKey:@(self.highestPriority)];
            [self startHustling];
        }
        return;
    }
    
    [self setIsBusy:true];
    
    request[@"status"] = @(1);
    NSMutableArray *ongoingRequests = [self.sortedRequests objectForKey:@"ongoingRequest"];
    if (!ongoingRequests) {
        NSMutableArray *ongoingRequests = [[NSMutableArray alloc]init];
        [self.sortedRequests setObject:ongoingRequests forKey:@"ongoingRequest"];
    }
    [ongoingRequests addObject:request];
    
    [highestPriorityRequestArray removeObject:request];
    if (highestPriorityRequestArray.count == 0) {
        [self.sortedRequests removeObjectForKey:@(self.highestPriority)];
        if (self.sortedRequests.count > 0) {
            NSInteger tempHighestPrio = 0;
            for (NSNumber *prio in self.sortedRequests.allKeys) {
                if (prio.integerValue > tempHighestPrio) {
                    tempHighestPrio = prio.integerValue;
                }
            }
            [self setHighestPriority:tempHighestPrio];
        }
    }
    
    _currentTotalPriorities += ((NSNumber*)request[@"priority"]).integerValue;
    
    __weak APIManager *weakSelf = self;
    [self requestApiWithHttpMethod:firstRequest.method url:firstRequest.requestUrl
                        parameters:firstRequest.parameters
                      customHeaders:firstRequest.headerData
                        completion:^(NSData *data, NSURLResponse *response, NSError *error) {
                            
                            [weakSelf setIsBusy:false];
                            
                            weakSelf.currentTotalPriorities -= ((NSNumber*)request[@"priority"]).integerValue;
                            
                            NSArray *completionBlocks = request[@"completionBlocks"];
                            for (APICompletionBlock completion in completionBlocks) {
                                completion(data, response, error);
                            }
                            
                            [weakSelf.requests removeObjectForKey:firstRequest.requestUrl.absoluteString];
                            [ongoingRequests removeObject:request];
                            [weakSelf startHustling];
                            
    }];
}

-(void)cancelAllRequests {
    for (NSMutableDictionary *request in requests.allValues) {
        NSMutableArray *hdRequests = request[@"requests"];
        for (HDRequest *hdRequest in hdRequests) {
            [hdRequest cancelRequest];
        }
    }
}

@end



















