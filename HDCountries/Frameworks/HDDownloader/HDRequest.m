//
//  HDRequest.m
//  HDDownloader
//
//  Created by Hussein Dimessi on 4/14/17.
//  Copyright © 2017 Hussein Dimessi. All rights reserved.
//

#import "HDRequest.h"
#import "APIManager.h"

@interface HDRequest () <HDRequestDelegate>

-(void)setStatus:(HDRequestStatus)status;
-(void)setRequestUrl:(NSURL*)requestUrl;
-(void)setPriority:(NSInteger)priority;
-(void)setDelegate:(id<HDRequestDelegate>)delegate;
-(void)setMethod:(HttpMethod)method;
-(void)setParameters:(NSDictionary*)parameters;

@end

@implementation HDRequest {
    CompletionBlock _completion;
}

@synthesize headerData;

+(void)load{
    
}

+(void)setMaxSimultaneousRequest:(NSInteger)max {
    [APIManager sharedManager].maxTotalPriorities = max;
}

+(void)cancelAllRequests {
    [[APIManager sharedManager] cancelAllRequests];
}

+(void)setTimeOut:(NSInteger)timeout {
    [APIManager.sharedManager setTimeout:timeout];
}

+(void)setMaxCacheSizeInBytes:(NSInteger)size {
    [[APIManager sharedManager].cache setTotalCostLimit:size];
}

-(id)init {
    self = [super init];
    [self setHeaderData:[APIManager sharedManager].headerData];
    self.status = HDRequestStatusIdle;
    self.priority = 1;
    return self;
}

///////////////////////////////

-(void)downloadImageWithURL:(NSURL *)url WithPriority:(NSNumber *)priority
                 completion:(ImageCompletionBlock) completion {
    [self verifyRequestWithURL:url completion:completion];
    UIImage *cachedImage = [[APIManager sharedManager].cache objectForKey:url.absoluteString];
    if (cachedImage && [cachedImage isKindOfClass:UIImage.class]) {
        
        completion(cachedImage, nil);
        return;
    }else{
        NSData *cachedData = [NSUserDefaults.standardUserDefaults objectForKey:url.absoluteString];
        if (cachedData && [UIImage imageWithData:cachedData]) {
            completion([UIImage imageWithData:cachedData], nil);
            return;
        }
    }
    self.method = GET;
    [self setupRequest:url parameters:nil WithPriority:priority completion:completion apiCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            completion(nil, error);
        }else{
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                [APIManager.sharedManager.cache setObject:image forKey:self.requestUrl.absoluteString
                                                     cost:data.length];
                [NSUserDefaults.standardUserDefaults setObject:data forKey:url.absoluteString];
                completion(image, nil);
            }else{
                NSString *errorDescription = @"Unsupported URL";
                NSError *err = [self errorWithDescription:errorDescription
                                                  andCode:HDRequestErrorUnsupportedURL];
                completion(nil,err);
            }
        }
    }];
}

-(void)downloadImageWithURL:(NSURL *)url completion:(ImageCompletionBlock) completion {
    [self downloadImageWithURL:url WithPriority:@(1) completion:completion];
}

///////////////////////////////

-(void)getJSONWithURL:(NSURL *)url parameters:(NSDictionary*)parameters
         WithPriority:(NSNumber *)priority completion:(JSONCompletionBlock) completion{
    [self verifyRequestWithURL:url completion:completion];
    self.method = GET;
    [self setupRequest:url parameters:parameters WithPriority:priority completion:completion apiCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            completion(nil, error);
        }else{
            NSError *err;
            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
            if (err) {
                NSString *errorDescription = @"Unsupported URL";
                NSError *err = [self errorWithDescription:errorDescription
                                                  andCode:HDRequestErrorUnsupportedURL];
                completion(nil,err);
            }else{
                completion(json,nil);
            }
        }
    }];
    
}

-(void)getJSONWithURL:(NSURL *)url parameters:(NSDictionary *)parameters completion:(JSONCompletionBlock)completion {
    [self getJSONWithURL:url parameters:parameters WithPriority:@(1) completion:completion];
}

///////////////////////////////

-(void)postJSONWithURL:(NSURL *)url parameters:(NSDictionary*)parameters
         WithPriority:(NSNumber *)priority completion:(JSONCompletionBlock) completion {
    [self verifyRequestWithURL:url completion:completion];
    self.method = POST;
    [self setupRequest:url parameters:parameters WithPriority:priority completion:completion apiCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            completion(nil, error);
        }else{
            NSError *err;
            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
            if (err) {
                NSString *errorDescription = @"Unsupported URL";
                NSError *err = [self errorWithDescription:errorDescription
                                                  andCode:HDRequestErrorUnsupportedURL];
                completion(nil,err);
            }else{
                completion(json,nil);
            }
        }
    }];
    
}

-(void)postJSONWithURL:(NSURL *)url parameters:(NSDictionary *)parameters completion:(JSONCompletionBlock)completion {
    [self postJSONWithURL:url parameters:parameters WithPriority:@(1) completion:completion];
}

///////////////////////////////

-(void)deleteJSONWithURL:(NSURL *)url parameters:(NSDictionary*)parameters
           completion:(JSONCompletionBlock) completion {
    [self verifyRequestWithURL:url completion:completion];
    self.method = DELETE;
    [self setupRequest:url parameters:parameters WithPriority:@(1) completion:completion apiCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            completion(nil, error);
        }else{
            NSError *err;
            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
            if (err) {
                NSString *errorDescription = @"Unsupported URL";
                NSError *err = [self errorWithDescription:errorDescription
                                                  andCode:HDRequestErrorUnsupportedURL];
                completion(nil,err);
            }else{
                completion(json,nil);
            }
        }
    }];
}

///////////////////////////////

-(void)downloadDataWithURL:(NSURL *)url parameters:(NSDictionary*)parameters
              WithPriority:(NSNumber *)priority completion:(DataCompletionBlock) completion{
    [self verifyRequestWithURL:url completion:completion];
    NSData *cachedData = [[APIManager sharedManager].cache objectForKey:url.absoluteString];
    if (cachedData && [cachedData isKindOfClass:NSData.class]) {
        
        completion(cachedData, nil);
        return;
    }else{
        NSData *cachedData = [NSUserDefaults.standardUserDefaults objectForKey:url.absoluteString];
        if (cachedData && [cachedData isKindOfClass:NSData.class]) {
            completion(cachedData, nil);
            return;
        }
    }
    self.method = GET;
    [self setupRequest:url parameters:parameters WithPriority:priority completion:completion apiCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        completion(data, error);
        
        if (data) {
            [APIManager.sharedManager.cache setObject:data forKey:self.requestUrl.absoluteString
                                                 cost:data.length];
            [NSUserDefaults.standardUserDefaults setObject:data forKey:url.absoluteString];
            completion(data, nil);
        }else{
            NSString *errorDescription = @"Unsupported URL";
            NSError *err = [self errorWithDescription:errorDescription
                                              andCode:HDRequestErrorUnsupportedURL];
            completion(nil,err);
        }
    }];
}

-(void)downloadDataWithURL:(NSURL *)url parameters:(NSDictionary*)parameters
               completion:(DataCompletionBlock) completion{
    [self downloadDataWithURL:url parameters:parameters WithPriority:@(1) completion:completion];
}

-(void)downloadDataWithURL:(NSURL *)url WithPriority:(NSNumber *)priority
                completion:(DataCompletionBlock) completion{
    [self downloadDataWithURL:url parameters:nil WithPriority:priority completion:completion];
}

-(void)downloadDataWithURL:(NSURL *)url completion:(DataCompletionBlock) completion {
    [self downloadDataWithURL:url parameters:nil WithPriority:@(1) completion:completion];
}

///////////////////////////////

-(void)verifyRequestWithURL:(NSURL*)url completion:(CompletionBlock)completion {
    if (!completion){
        return;
    }
    if (self.status == HDRequestStatusDownloading) {
        NSString *errorDescription = @"Request already downloading";
        NSError *error = [self errorWithDescription:errorDescription
                                            andCode:HDRequestErrorAlreadyDownloading];
        completion(nil,error);
        return;
    }else if (!url) {
        NSString *errorDescription = @"Unsupported URL";
        NSError *error = [self errorWithDescription:errorDescription
                                            andCode:HDRequestErrorUnsupportedURL];
        completion(nil,error);
    }
}

-(void)setupRequest:(NSURL *)url parameters:(NSDictionary*)parameters
       WithPriority:(NSNumber *)priority completion:(CompletionBlock) completion apiCompletion:(APICompletionBlock) apiCompletion{
    
    dispatch_queue_t current = [NSOperationQueue currentQueue].underlyingQueue;
    _completion = completion;
    self.priority = priority.integerValue;
    self.status = HDRequestStatusPending;
    self.requestUrl = url;
    self.parameters = parameters;
    self.delegate = self;
    [[APIManager sharedManager] appendNewRequest:self completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        if (self.status == HDRequestStatusDownloading) {
            self.status = HDRequestStatusFinished;
        }else{
            return;
        }
        
        dispatch_async(current, ^{
            apiCompletion(data, response, error);
        });
        
    }];
}

-(void)cancelRequest {
    if (self.status == HDRequestStatusDownloading || self.status == HDRequestStatusPending) {
        self.status = HDRequestStatusCanceled;
        NSString *errorDescription = @"request canceled";
        NSError *err = [self errorWithDescription:errorDescription
                                          andCode:HDRequestErrorCancelled];
        _completion(nil,err);
    }
}

#pragma mark - Setters

-(void)setStatus:(HDRequestStatus)status{
    _status = status;
}

-(void)setPriority:(NSInteger)priority{
    if (priority > 10) {
        _priority = 10;
    }else if (priority < 1) {
        _priority = 1;
    }else{
        _priority = priority;
    }
}

-(void)setRequestUrl:(NSURL*)requestUrl {
    _requestUrl = requestUrl;
}

-(void)setDelegate:(id<HDRequestDelegate>)delegate {
    _delegate = delegate;
}

-(void)setMethod:(HttpMethod)method{
    _method = method;
}

-(void)setParameters:(NSDictionary*)parameters {
    _parameters = parameters;
}

#pragma mark - HDRequestDelegate

-(void)requestBeganDownloading {
    self.status = HDRequestStatusDownloading;
}

#pragma mark - Helpers

-(NSError *)errorWithDescription:(NSString *)errorDescription andCode:(NSInteger)code {
    NSError *error;
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    [details setValue:errorDescription forKey:NSLocalizedDescriptionKey];
    error = [NSError errorWithDomain:@"HDDownloader" code:code userInfo:details];
    return error;
}

@end
