//
//  main.m
//  HDCountries
//
//  Created by Hussein Dimessi on 05/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
