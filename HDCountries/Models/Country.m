//
//  Country.m
//  HDCountries
//
//  Created by Hussein Dimessi on 06/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import "Country.h"

@implementation Country

+ (NSArray *)getAllCountriesFromDatabase{
    NSEntityDescription *entityDesc =[NSEntityDescription entityForName:@"Country"inManagedObjectContext:APP_DELEGATE.persistentContainer.viewContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSArray *allCountries = [APP_DELEGATE.persistentContainer.viewContext executeFetchRequest:request error:nil];
    return allCountries;
}

+ (NSMutableArray *) parseCountries:(NSArray *)jsonArray{
    NSMutableArray *countries = [[NSMutableArray alloc]init];
    for (NSDictionary *dic in jsonArray) {
        Country *country =  [NSEntityDescription insertNewObjectForEntityForName:@"Country"
                                                          inManagedObjectContext:APP_DELEGATE.persistentContainer.viewContext];
        
        country.name            = [dic objectForKey:@"name"] ;
        country.region            = [dic objectForKey:@"region"] ;
        country.capital            = [dic objectForKey:@"capital"];
        country.flagSvgLink            = [dic objectForKey:@"flag"];
        country.population            = ((NSNumber*)[dic valueForKey:@"population"]).doubleValue;
        if (![dic valueForKey:@"area"] || [[dic valueForKey:@"area"] isKindOfClass:NSNull.class]) {
            continue;
        }
        country.area            = ((NSNumber*)[dic valueForKey:@"area"]).doubleValue;
        
        if ([[dic objectForKey:@"alpha2Code"] isKindOfClass:NSString.class]) {
            NSString *alphaCode = [[dic objectForKey:@"alpha2Code"] lowercaseString];
            country.flagLink            = [NSString stringWithFormat:@"https://raw.githubusercontent.com/hjnilsson/country-flags/master/png250px/%@.png", alphaCode] ;
        }
        
        
        NSArray *coordinateArray = [dic objectForKey:@"latlng"];
        if (coordinateArray && coordinateArray.count == 2) {
            country.latitude            = ((NSNumber*)coordinateArray[0]).doubleValue;
            country.longitude            = ((NSNumber*)coordinateArray[1]).doubleValue;
        }
        [countries addObject:country];
    }
    return countries;
}

+ (void) saveAllCountries:(NSArray *)countriesArray{
    
    NSError *error;
    [APP_DELEGATE.persistentContainer.viewContext save:&error];
    if (error) {
        NSLog(@"error: %@ \n\t failed to save the countries",error);
    }
}



+ (void)clearAllCountries{
    for (Country *country in [self getAllCountriesFromDatabase]) {
        [APP_DELEGATE.persistentContainer.viewContext deleteObject:country];
    }
    NSError *error;
    [APP_DELEGATE.persistentContainer.viewContext save:&error];
    if (error) {
        NSLog(@"failed to clear database, error: %@", error);
    }
}



@end
