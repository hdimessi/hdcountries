//
//  Country+CoreDataProperties.m
//  HDCountries
//
//  Created by Hussein Dimessi on 06/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import "Country+CoreDataProperties.h"

@implementation Country (CoreDataProperties)

@dynamic name;
@dynamic region;
@dynamic capital;
@dynamic latitude;
@dynamic longitude;
@dynamic population;
@dynamic flagLink;
@dynamic flagSvgLink;
@dynamic area;
@dynamic bookmarked;

@end
