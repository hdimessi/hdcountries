//
//  Country.h
//  HDCountries
//
//  Created by Hussein Dimessi on 06/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


NS_ASSUME_NONNULL_BEGIN

@interface Country : NSManagedObject

+ (NSMutableArray *) parseCountries:(NSArray *)jsonArray;
+ (void) saveAllCountries:(NSArray *)jsonArray;
+ (void)clearAllCountries;
+ (NSArray *)getAllCountriesFromDatabase;

@end

NS_ASSUME_NONNULL_END

#import "Country+CoreDataProperties.h"
