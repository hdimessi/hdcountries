//
//  Country+CoreDataProperties.h
//  HDCountries
//
//  Created by Hussein Dimessi on 06/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import "Country.h"

NS_ASSUME_NONNULL_BEGIN

@interface Country (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *capital;
@property (nullable, nonatomic, retain) NSString *region;
@property (nullable, nonatomic, retain) NSString *flagLink;
@property (nullable, nonatomic, retain) NSString *flagSvgLink;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nonatomic) int32_t population;
@property (nonatomic) int32_t area;
@property (nonatomic) BOOL bookmarked;

@end

NS_ASSUME_NONNULL_END
