//
//  BookmarkAnimationView.m
//  HDCountries
//
//  Created by Hussein Dimessi on 05/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import "BookmarkAnimationView.h"

@implementation BookmarkAnimationView

-(instancetype)init{
    
    CGRect frame = CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
    self = [self initWithFrame:frame];
    
    self.backgroundColor = [UIColor clearColor];
    UIView *background = [[UIView alloc]initWithFrame:self.frame];
    background.backgroundColor = [UIColor orangeColor];
    background.alpha = 0.8;
    [self addSubview:background];
    
    _starIcon = [[UIImageView alloc]initWithFrame:CGRectMake((SCREEN_SIZE.width - 200) / 2, (SCREEN_SIZE.height - 200) / 2, 200, 180)];
    _starIcon.image = [UIImage imageNamed:@"bookmark"];
    _starIcon.hidden = true;
    [self addSubview:_starIcon];
    
    return self;
}

-(void)bookmarkIt {
    
    UIViewController *rootVC = UIApplication.sharedApplication.keyWindow.rootViewController;
    [rootVC.view addSubview:self];
    
    self.alpha = 1;
    
    UIBezierPath *animationPath = [[UIBezierPath alloc]init];
    [animationPath moveToPoint:CGPointMake((SCREEN_SIZE.width - 200) / 2 + 5, (SCREEN_SIZE.height - 200) / 2 + 65)];
    [animationPath addLineToPoint:CGPointMake((SCREEN_SIZE.width - 200) / 2 + 75, (SCREEN_SIZE.height - 200) / 2 + 60)];
    [animationPath addArcWithCenter:CGPointMake((SCREEN_SIZE.width - 200) / 2 + 100, (SCREEN_SIZE.height - 200) / 2 + 5) radius:5 startAngle:12 * M_PI / 10 endAngle:18 * M_PI / 10 clockwise:true];
    [animationPath addLineToPoint:CGPointMake((SCREEN_SIZE.width - 200) / 2 + 125, (SCREEN_SIZE.height - 200) / 2 + 60)];
    [animationPath addArcWithCenter:CGPointMake((SCREEN_SIZE.width - 200) / 2 + 195, (SCREEN_SIZE.height - 200) / 2 + 70) radius:5 startAngle:-M_PI / 2 endAngle:7 * M_PI / 20 clockwise:true];
    [animationPath addLineToPoint:CGPointMake((SCREEN_SIZE.width - 200) / 2 + 150, (SCREEN_SIZE.height - 200) / 2 + 115)];
    [animationPath addArcWithCenter:CGPointMake((SCREEN_SIZE.width - 200) / 2 + 160, (SCREEN_SIZE.height - 200) / 2 + 175) radius:5 startAngle:-8 * M_PI / 60 endAngle:4 * M_PI / 6 clockwise:true];
    [animationPath addLineToPoint:CGPointMake((SCREEN_SIZE.width - 200) / 2 + 100, (SCREEN_SIZE.height - 200) / 2 + 140)];
    [animationPath addArcWithCenter:CGPointMake((SCREEN_SIZE.width - 200) / 2 + 40, (SCREEN_SIZE.height - 200) / 2 + 175) radius:5 startAngle:10 * M_PI / 40 endAngle:64 * M_PI / 60 clockwise:true];
    [animationPath addLineToPoint:CGPointMake((SCREEN_SIZE.width - 200) / 2 + 50, (SCREEN_SIZE.height - 200) / 2 + 115)];
    [animationPath addArcWithCenter:CGPointMake((SCREEN_SIZE.width - 200) / 2 + 5, (SCREEN_SIZE.height - 200) / 2 + 70) radius:5 startAngle:14 * M_PI / 20 endAngle:-M_PI / 2 clockwise:true];
    
    
    CAShapeLayer *animationLayer = [[CAShapeLayer alloc]init];
    animationLayer.path = animationPath.CGPath;
    animationLayer.fillColor = [UIColor clearColor].CGColor;
    animationLayer.strokeColor = [UIColor whiteColor].CGColor;
    animationLayer.lineWidth = 2.0;
    animationLayer.strokeEnd = 0.0;
    [self.layer addSublayer:animationLayer];
    
    animationLayer.opacity = 1;
    
    CABasicAnimation *theActualAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    theActualAnimation.duration = 1;
    theActualAnimation.fromValue = [NSNumber numberWithInt:0];
    theActualAnimation.toValue = [NSNumber numberWithInt:1];
    theActualAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    animationLayer.strokeEnd = 1;
    [animationLayer addAnimation:theActualAnimation forKey:@"bookmarkAnimation"];
    
    [NSTimer scheduledTimerWithTimeInterval:1 repeats:false block:^(NSTimer * _Nonnull timer) {
        [animationLayer removeFromSuperlayer];
        UIView *flashView = [[UIView alloc]initWithFrame:self.frame];
        flashView.backgroundColor = [UIColor whiteColor];
        [self addSubview:flashView];
        _starIcon.hidden = false;
        _starIcon.transform = CGAffineTransformMakeScale(2, 2);
        [UIView animateWithDuration:0.4 animations:^{
            _starIcon.transform = CGAffineTransformMakeScale(0.9, 0.9);
            flashView.alpha = 0;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3 animations:^{
                _starIcon.transform = CGAffineTransformMakeScale(1.1, 1.1);
                flashView.alpha = 0;
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3 animations:^{
                    _starIcon.transform = CGAffineTransformMakeScale(0.9, 0.9);
                    flashView.alpha = 0;
                } completion:^(BOOL finished) {
                   [UIView animateWithDuration:0.3 animations:^{
                       _starIcon.transform = CGAffineTransformIdentity;
                       flashView.alpha = 0;
                   } completion:^(BOOL finished) {
                       [UIView animateWithDuration:0.3 animations:^{
                           self.alpha = 0;
                       } completion:^(BOOL finished) {
                           [self removeFromSuperview];
                       }];
                   }];
                }];
            }];
        }];
        
        CABasicAnimation *opacityAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        opacityAnimation.duration = 3;
        opacityAnimation.fromValue = [NSNumber numberWithInt:1];
        opacityAnimation.toValue = [NSNumber numberWithInt:0];
        opacityAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        animationLayer.opacity = 0;
        [animationLayer addAnimation:opacityAnimation forKey:@"fadeout"];
        
    }];
}

@end

























