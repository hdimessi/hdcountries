//
//  HGCFlagholderIndicator.m
//  HDCountries
//
//  Created by Hussein Dimessi on 07/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import "HGCFlagholderIndicator.h"

@implementation HGCFlagholderIndicator

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    for (UIView *subv in self.subviews) {
        [subv removeFromSuperview];
    }
    self.frame = frame;
    self.backgroundColor = [UIColor clearColor];
    [self setUserInteractionEnabled:false];
    
    UIView *middleDot = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
    middleDot.center = self.center;
    middleDot.layer.cornerRadius = 2.5;
    middleDot.backgroundColor = [UIColor whiteColor];
    UIView *leftDot = [[UIView alloc]initWithFrame:CGRectMake(middleDot.frame.origin.x - 30, middleDot.frame.origin.y, 10, 10)];
    leftDot.layer.cornerRadius = 2.5;
    leftDot.backgroundColor = [UIColor whiteColor];
    UIView *rightDot = [[UIView alloc]initWithFrame:CGRectMake(middleDot.frame.origin.x + 30, middleDot.frame.origin.y, 10, 10)];
    rightDot.layer.cornerRadius = 2.5;
    rightDot.backgroundColor = [UIColor whiteColor];
    [self addSubview:middleDot];
    [self addSubview:leftDot];
    [self addSubview:rightDot];
    [self animateDotAtIndex:0];
    [NSTimer scheduledTimerWithTimeInterval:0.3 repeats:false block:^(NSTimer * _Nonnull timer) {
        [self animateDotAtIndex:1];
    }];
    [self setHidden:true];
    return self;
}

-(void)startLoading {
    [self setHidden:false];
}

-(void)animateDotAtIndex:(NSInteger)index {
    if (self.subviews.count == 0) {
        return;
    }
    UIView *dot = self.subviews[index];
    [UIView animateWithDuration:0.3 animations:^{
        dot.frame = CGRectMake(dot.frame.origin.x,
                                   dot.frame.origin.y - 20,
                                   dot.frame.size.width,
                                   dot.frame.size.height);
        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            dot.frame = CGRectMake(dot.frame.origin.x,
                                   dot.frame.origin.y + 20,
                                   dot.frame.size.width,
                                   dot.frame.size.height);
        } completion:^(BOOL finished) {
            NSInteger newIndex = index + 2;
            if (newIndex > self.subviews.count - 1) {
                newIndex = newIndex - self.subviews.count;
            }
            [self animateDotAtIndex:newIndex];
        }];
    }];
}

-(void)stop {
    [self setHidden:true];
}


@end
