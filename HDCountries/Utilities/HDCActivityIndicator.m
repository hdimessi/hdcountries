//
//  HDCActivityIndicator.m
//  HDCountries
//
//  Created by Hussein Dimessi on 06/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import "HDCActivityIndicator.h"

@implementation HDCActivityIndicator

+ (HDCActivityIndicator *)shared {
    static HDCActivityIndicator *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

-(instancetype)init{
    
    CGRect frame = CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
    self = [self initWithFrame:frame];
    
    self.backgroundColor = [UIColor clearColor];
    [self setUserInteractionEnabled:false];
    
    return self;
}

-(void)startLoadingInView:(UIView*)view {
    if (self.superview) {
        NSLog(@"already Loading");
        return;
    }
    
    [view addSubview:self];
    
    self.alpha = 1;
    [self setTransform:CGAffineTransformMakeRotation(0)];
    
    UIBezierPath *animationPath = [[UIBezierPath alloc]init];
    [animationPath moveToPoint:CGPointMake((SCREEN_SIZE.width - 20) / 2, (SCREEN_SIZE.height - 40) / 2 + 37)];
    [animationPath addLineToPoint:CGPointMake((SCREEN_SIZE.width - 20) / 2 , (SCREEN_SIZE.height - 40) / 2 )];
    [animationPath addLineToPoint:CGPointMake((SCREEN_SIZE.width - 20) / 2 + 11 , (SCREEN_SIZE.height - 40) / 2 )];
    [animationPath addArcWithCenter:CGPointMake((SCREEN_SIZE.width - 20) / 2 + 11, (SCREEN_SIZE.height - 40) / 2 + 8) radius:8 startAngle:-M_PI / 2 endAngle: M_PI / 2 clockwise:true];
    [animationPath addLineToPoint:CGPointMake((SCREEN_SIZE.width - 20) / 2 , (SCREEN_SIZE.height - 40) / 2 + 16 )];
    [animationPath addLineToPoint:CGPointMake((SCREEN_SIZE.width - 20) / 2 + 11 , (SCREEN_SIZE.height - 40) / 2 + 16 )];
    [animationPath addLineToPoint:CGPointMake((SCREEN_SIZE.width - 20) / 2 + 22 , (SCREEN_SIZE.height - 40) / 2 + 36 )];
    
    
    CAShapeLayer *animationLayer = [[CAShapeLayer alloc]init];
    animationLayer.path = animationPath.CGPath;
    animationLayer.fillColor = [UIColor clearColor].CGColor;
    animationLayer.strokeColor = [UIColor orangeColor].CGColor;
    animationLayer.lineWidth = 4.5;
    animationLayer.strokeEnd = 0.0;
    [self.layer addSublayer:animationLayer];
    
    animationLayer.opacity = 1;
    
    CABasicAnimation *theActualAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    theActualAnimation.duration = 1;
    theActualAnimation.fromValue = [NSNumber numberWithInt:0];
    theActualAnimation.toValue = [NSNumber numberWithInt:1];
    theActualAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    animationLayer.strokeEnd = 1;
    [animationLayer addAnimation:theActualAnimation forKey:@"bookmarkAnimation"];
    
    [NSTimer scheduledTimerWithTimeInterval:1.1 repeats:false block:^(NSTimer * _Nonnull timer) {
        if (!self.superview) {
            return ;
        }
        [UIView animateWithDuration:0.5f animations:^{
            [self setTransform:CGAffineTransformMakeRotation(M_PI)];
        } completion:^(BOOL finished) {
            if (!self.superview) {
                return ;
            }
            UIView *circle = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 60, 60)];
            circle.center = CGPointMake(self.superview.center.x - 2, self.superview.center.y - 1);
            circle.layer.cornerRadius = circle.frame.size.height / 2;
            circle.backgroundColor = [UIColor clearColor];
            circle.layer.borderWidth = 1;
            circle.layer.borderColor = [UIColor orangeColor].CGColor;
            circle.alpha = 0;
            [self addSubview:circle];
            [self pulseAnimation:circle];
            
        }];
        
    }];
}

-(void)pulseAnimation:(UIView*)circle {
    [UIView animateWithDuration:0.6 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        circle.transform = CGAffineTransformMakeScale(1.6, 1.6);
        circle.alpha = 0.5;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.6 animations:^{
            circle.transform = CGAffineTransformIdentity;
            circle.alpha = 1;
        } completion:^(BOOL finished) {
            [self pulseAnimation:circle];
        }];
        
    }];
}

-(void)stop {
    [self removeFromSuperview];
    for (UIView *subv in self.subviews) {
        [subv removeFromSuperview];
    }
    
    for (CALayer *subL in self.layer.sublayers) {
        [subL removeFromSuperlayer];
    }
}

@end
