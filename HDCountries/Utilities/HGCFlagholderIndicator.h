//
//  HGCFlagholderIndicator.h
//  HDCountries
//
//  Created by Hussein Dimessi on 07/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGCFlagholderIndicator : UIView

-(void)startLoading;
-(void)stop;

@end
