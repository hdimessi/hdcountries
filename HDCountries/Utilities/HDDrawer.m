//
//  HDDrawer.m
//  HDCountries
//
//  Created by Hussein Dimessi on 05/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import "HDDrawer.h"

@interface HDDrawer ()

@property (nonatomic) BOOL menuHidden;
@property UISwipeGestureRecognizer *swipeGesture;

@end

@implementation HDDrawer

+(HDDrawer*)createDrawerWithMenu:(UIViewController*)menu width:(CGFloat)width {
    
    HDDrawer *drawer = [[HDDrawer alloc]initWithMenu:menu andWidth:width];
    
    return drawer;
}

-(instancetype)initWithMenu:(UIViewController*)menu andWidth:(CGFloat)width {
    self = [self init];
    
    self.view.frame = CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
    
    self.mainView = [[UIView alloc]initWithFrame:self.view.frame];
    [self.view addSubview:self.mainView];
    
    _menuVC = menu;
    _menuVC.view.frame = CGRectMake(-width, 0, width, SCREEN_SIZE.height);
    [self addChildViewController:_menuVC];
    [self.view addSubview:_menuVC.view];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
    blurView.frame = CGRectMake(0, 0, width, SCREEN_SIZE.height);
    blurView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [_menuVC.view addSubview:blurView];
    [_menuVC.view sendSubviewToBack:blurView];
    
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _navigationBranches = [[NSMutableDictionary alloc]init];
    _swipeGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiped)];
    [_swipeGesture setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:_swipeGesture];
}

-(void)setMenuHidden:(BOOL)menuHidden {
    _menuHidden = menuHidden;
    
    if (_menuHidden) {
        [_swipeGesture setEnabled:false];
    }else{
        [_swipeGesture setEnabled:true];
    }
    
}

-(void)setNavigationBranch:(UIViewController*)viewController withName:(NSString*)name {
    UINavigationController *navigationBranch = [[UINavigationController alloc]initWithRootViewController:viewController];
    [navigationBranch.navigationBar setHidden:true];
    [self addChildViewController:navigationBranch];
    _navigationBranches[name] = navigationBranch;
}

-(void)openNavigationBranch:(NSString*)name {
    if (!_navigationBranches[name]) {
        NSLog(@"wrong navigation name %@", name);
        return;
    }
    
    UIViewController *branch = _navigationBranches[name];
    
    if (_mainView.subviews.count > 0) {
        [_mainView.subviews[0] removeFromSuperview];
    }
    [branch viewWillAppear:false];
    [_mainView addSubview:branch.view];
    
    [self hideMenu];
}

-(void)openMenu {
    [self.view layoutIfNeeded];
    [_menuVC viewWillAppear:true];
    
    [UIView animateWithDuration:0.3 animations:^{
        _menuVC.view.frame = CGRectMake(0, 0, _menuVC.view.frame.size.width, _menuVC.view.frame.size.height);
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        if (finished) {
            _menuHidden = false;
        }
    }];
}

-(void)hideMenu {
    [_menuVC viewWillDisappear:true];
    CGFloat menuWidth = _menuVC.view.frame.size.width;
    CGFloat menuHeight = _menuVC.view.frame.size.height;
    [UIView animateWithDuration:0.3 animations:^{
        _menuVC.view.frame = CGRectMake(-menuWidth, 0, menuWidth, menuHeight);
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        _menuHidden = true;
    }];
}

-(void)swiped {
    [self hideMenu];
}

@end
