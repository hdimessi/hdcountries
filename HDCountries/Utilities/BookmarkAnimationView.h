//
//  BookmarkAnimationView.h
//  HDCountries
//
//  Created by Hussein Dimessi on 05/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookmarkAnimationView : UIView

@property UIImageView *starIcon;

-(void)bookmarkIt;

@end
