//
//  HDCActivityIndicator.h
//  HDCountries
//
//  Created by Hussein Dimessi on 06/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HDCActivityIndicator : UIView

+(HDCActivityIndicator*)shared;

-(void)startLoadingInView:(UIView*)view;
-(void)stop;

@end
