//
//  HDDrawer.h
//  HDCountries
//
//  Created by Hussein Dimessi on 05/06/2017.
//  Copyright © 2017 hussein dimessi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HDDrawer : UIViewController <UIGestureRecognizerDelegate>

@property UIView *mainView;
@property (readonly) UIViewController *menuVC;
@property (readonly) NSMutableDictionary *navigationBranches;

+(HDDrawer*)createDrawerWithMenu:(UIViewController*)menu width:(CGFloat)width;

-(void)setNavigationBranch:(UIViewController*)viewController withName:(NSString*)name;

-(void)openNavigationBranch:(NSString*)name;
-(void)openMenu;
-(void)hideMenu;

@end
